import numpy as np
import scipy
import scipy.optimize


def sound_speed(p, rho, gamma=1.4, dustFrac=0):
    """
    Calculate speed of sound according to

        .. math::
            c = \sqrt{\gamma \frac{p}{\rho} (1-\theta)}
    where :math:`p` is pressure, :math:`\rho` is density, :math:`\gamma` is heat capacity ratio
    and :math:`\theta` is dust fraction, according to Tricco, Price and Laibe, 2017

    :rtype : float
    :return: returns the speed of sound
    """
    return np.sqrt(gamma * (1-dustFrac) * p / rho)


def calc_positions(pl, pr, xi, w, t, region1, region3):
    """
    :return: tuple of positions in the following order ->
            Head of Rarefaction: xhd,  Foot of Rarefaction: xft,
            Contact Discontinuity: xcd, Shock: xsh
    """
    p1, rho1 = region1[:2]  # don't need velocity
    p3, rho3, u3 = region3[:]
    c1 = sound_speed(p1, rho1)
    c3 = sound_speed(p3, rho3)
    if pl > pr:
        xsh = xi + w * t
        xcd = xi + u3 * t
        xft = xi + (u3 - c3) * t
        xhd = xi - c1 * t
    else:
        # pr > pl
        xsh = xi - w * t
        xcd = xi - u3 * t
        xft = xi - (u3 - c3) * t
        xhd = xi + c1 * t

    return xhd, xft, xcd, xsh


def region_states(pl, pr, region1, region3, region4, region5):
    """
    :return: dictionary (region no.: p, rho, u), except for rarefaction region
    where the value is a string, obviously
    """
    if pl > pr:
        return {'Region 1': region1,
                'Region 2': 'RAREFACTION',
                'Region 3': region3,
                'Region 4': region4,
                'Region 5': region5}
    else:
        return {'Region 1': region5,
                'Region 2': region4,
                'Region 3': region3,
                'Region 4': 'RAREFACTION',
                'Region 5': region1}


def create_arrays(pl, pr, region1, region3, region4, region5, gamma, xl, xi, xr, t, npts, positions):
    """
    :return: tuple of x, p, rho and u values across the domain of interest
    """
    xhd, xft, xcd, xsh = positions
    p1, rho1, u1 = region1
    p3, rho3, u3 = region3
    p4, rho4, u4 = region4
    p5, rho5, u5 = region5
    gm1 = gamma - 1.
    gp1 = gamma + 1.

    x_arr = np.linspace(xl, xr, npts)
    rho = np.zeros(npts, dtype=float)
    p = np.zeros(npts, dtype=float)
    u = np.zeros(npts, dtype=float)
    c1 = sound_speed(p1, rho1)

    if pl > pr:
        for i, x in enumerate(x_arr):
            if x < xhd:
                u[i] = u1
                rho[i] = rho1
                p[i] = p1
            elif x < xft:
                u[i] = 2. / gp1 * (c1 + (x - xi) / t)
                fact = 1. - 0.5 * gm1 * u[i] / c1
                rho[i] = rho1 * fact ** (2. / gm1)
                p[i] = p1 * fact ** (2. * gamma / gm1)
            elif x < xcd:
                u[i] = u3
                rho[i] = rho3
                p[i] = p3
            elif x < xsh:
                u[i] = u4
                rho[i] = rho4
                p[i] = p4
            else:
                u[i] = u5
                rho[i] = rho5
                p[i] = p5
    else:
        for i, x in enumerate(x_arr):
            if x < xsh:
                u[i] = -u1
                rho[i] = rho5
                p[i] = p5
            elif x < xcd:
                u[i] = -u4
                rho[i] = rho4
                p[i] = p4
            elif x < xft:
                u[i] = -u3
                rho[i] = rho3
                p[i] = p3
            elif x < xhd:
                u[i] = -2. / gp1 * (c1 + (xi - x) / t)
                fact = 1. + 0.5 * gm1 * u[i] / c1
                rho[i] = rho1 * fact ** (2. / gm1)
                p[i] = p1 * fact ** (2. * gamma / gm1)
            else:
                u[i] = -u1
                rho[i] = rho1
                p[i] = p1

    return x_arr, p, rho, u


def shock_tube_function(gamma, p4, p1, p5, rho1, rho5):
    """
    Shock tube equation
    """
    z = (p4 / p5 - 1.)
    c1 = sound_speed(p1, rho1)
    c5 = sound_speed(p5, rho5)

    gm1 = gamma - 1.
    gp1 = gamma + 1.
    g2 = 2. * gamma

    fact = gm1 / g2 * (c5 / c1) * z / np.sqrt(1. + gp1 / g2 * z)
    fact = (1. - fact) ** (g2 / gm1)

    return p1 * fact - p4


def calculate_regions(pl, rhol, ul, pr, rhor, ur, gamma):
    """
    Compute regions
    :rtype : tuple
    :return: returns p, rho and u for regions 1,3,4,5 as well as the shock speed
    """
    if pl < pr:
        rho1 = rhor
        p1 = pr
        u1 = ur
        rho5 = rhol
        p5 = pl
        u5 = ul
    else:
        rho1 = rhol
        p1 = pl
        u1 = ul
        rho5 = rhor
        p5 = pr
        u5 = ur
    # solve for post-shock pressure
    # just in case the shock_tube_function gets a complex number
    num_of_guesses = 100
    for pguess in np.linspace(pr, pl, num_of_guesses):
        res = scipy.optimize.fsolve(shock_tube_function, pguess, args=(
            gamma, p1, p5, rho1, rho5), full_output=True)
        p4, infodict, ier, mesg = res
        if ier == 1:
            break
    if not ier == 1:
        raise Exception("Analytical Sod solution unsuccessful!")

    # chera bayad hamchin if E vojud dashte bashe???
    if type(p4) is np.ndarray:
        p4 = p4[0]

    # compute post-shock density and velocity
    z = (p4 / p5 - 1.)
    c5 = sound_speed(p5, rho5)

    gm1 = gamma - 1.
    gp1 = gamma + 1.
    gmfac1 = 0.5 * gm1 / gamma
    gmfac2 = 0.5 * gp1 / gamma

    fact = np.sqrt(1. + gmfac2 * z)

    u4 = c5 * z / (gamma * fact)
    rho4 = rho5 * (1. + gmfac2 * z) / (1. + gmfac1 * z)

    # shock speed
    w = c5 * fact

    # compute values at foot of rarefaction
    p3 = p4
    u3 = u4
    rho3 = rho1 * (p3 / p1) ** (1. / gamma)
    return (p1, rho1, u1), (p3, rho3, u3), (p4, rho4, u4), (p5, rho5, u5), w


def solve2(pl, pr, xl, xi, xr, w, t, gamma, npts, region1, region3, region4, region5):
    """
    Actually solves the sod shock tube problem
    :return:
    """
    regions = region_states(
        pl, pr, region1, region3, region4, region5)

    # calculate positions
    x_positions = calc_positions(
        pl, pr, xi, w, t, region1, region3)

    pos_description = ('Head of Rarefaction', 'Foot of Rarefaction',
                       'Contact Discontinuity', 'Shock')
    positions = dict(zip(pos_description, x_positions))

    # create arrays
    x, p, rho, u = create_arrays(
        pl, pr, region1, region3, region4, region5, gamma, xl, xi, xr, t, npts, x_positions)

    val_names = ('x', 'p', 'rho', 'u')
    val_dict = dict(zip(val_names, (x, p, rho, u)))

    return positions, regions, val_dict


def solve(left_state=(1, 1, 0), right_state=(0.1, 0.125, 0.), geometry=(-0.5, 0.5, 0), t=0.2, **kwargs):
    """
    Solves the Sod shock tube problem (i.e. riemann problem) of discontinuity across an interface.

    :rtype : tuple
    :param left_state: tuple (pl, rhol, ul)
    :param right_state: tuple (pr, rhor, ur)
    :param geometry: tuple (xl, xr, xi): xl - left boundary, xr - right boundary, xi - initial discontinuity
    :param t: time for which the states have to be calculated
    :param gamma: ideal gas constant, default is air: 1.4
    :param npts: number of points for array of pressure, density and velocity
    :param dustFrac: dust to gas fraction, should be >=
    :return: tuple of: dicts of positions,
    constant pressure, density and velocity states in distinct regions,
    arrays of pressure, density and velocity in domain bounded by xl, xr
    """

    pl, rhol, ul = left_state
    pr, rhor, ur = right_state
    xl, xr, xi = geometry
    # basic checking
    if xl >= xr:
        print('xl has to be less than xr!')
        exit()
    if xi >= xr or xi <= xl:
        print('xi has in between xl and xr!')
        exit()

    if 'npts' in kwargs:
        npts = kwargs['npts']
    else:
        npts = 500

    if 'gamma' in kwargs:
        gamma = kwargs['gamma']
    else:
        gamma = 1.4

    if 'dustFrac' in kwargs:
        # dustFrac = np.min(np.max(kwargs['dustFrac'], 0), 1)
        dustFrac = kwargs['dustFrac']
        if dustFrac < 0 or dustFrac >= 1:
            print('Invalid dust fraction value: {}. Should be >=0 and <1. Set to default: 0'.format(
                dustFrac))
            dustFrac = 0
    else:
        dustFrac = 0

    # calculate regions
    region1, region3, region4, region5, w = \
        calculate_regions(pl, rhol, ul, pr, rhor, ur, gamma)

    return solve2(pl, pr, xl, xi, xr, w, t, gamma, npts, region1, region3, region4, region5)
