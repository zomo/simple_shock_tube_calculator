import sys

sys.path.append('../shocktubecalc')
import sod
import matplotlib.pyplot as plt
import numpy as np

def standard_sod_shock_tube():
    obj = sod.Calculator(left_state=(1, 1, 0), right_state=(0.1, 0.125, 0.),
                         geometry=(-0.5, 0.5, 0), t=0.2, gamma=1.4)

    positions, regions, values = obj.solve()
    print('Positions:')
    for desc, vals in positions.items():
        print('{0:10} : {1}'.format(desc, vals))

    print('States:')
    for desc, vals in regions.items():
        print('{0:10} : {1}'.format(desc, vals))

    e = values['p'] / (values['rho'] * (5 / 3 - 1))
    T = values['p'] / values['rho']
    c = np.sqrt(5 / 3 * values['p'] / values['rho'])
    M = values['u'] / c


    plt.figure(1)
    plt.plot(values['x'], values['p'], linewidth=1.5, color="C0")
    plt.ylabel('pressure')
    plt.xlabel('x')
    plt.show()

    plt.figure(2)
    plt.plot(values['x'], values['rho'], linewidth=1.5, color="C1")
    plt.ylabel('density')
    plt.xlabel('x')
    plt.show()

    plt.figure(3)
    plt.plot(values['x'], values['u'], linewidth=1.5, color="C2")
    plt.ylabel('velocity')
    plt.xlabel('x')
    plt.show()

    plt.figure(4)
    plt.plot(values['x'], e, linewidth=1.5, color="C3")
    plt.ylabel('energy')
    plt.xlabel('x')
    plt.show()

    plt.figure(5)
    plt.plot(values['x'], T, linewidth=1.5, color="C4")
    plt.ylabel('temperature')
    plt.xlabel('x')
    plt.show()


    plt.figure(6)
    plt.plot(values['x'], M, linewidth=1.5, color="C5")
    plt.ylabel('Mach')
    plt.xlabel('x')
    plt.show()

def combine_standard_dust_fraction_shock_tube():
    dustFrac = 0.5
    stand_pos, stand_regions, stand_val = sod.solve(left_state=(1, 1, 0), right_state=(0.1, 0.125, 0.),
                                                    geometry=(-0.5, 0.5, 0), t=0.2, gamma=5 / 3)

    frac_pos, frac_regions, frac_val = sod.solve(left_state=(1, 1, 0), right_state=(0.1, 0.125, 0.),
                                                 geometry=(-0.5, 0.5, 0), t=0.2, gamma=5 / 3, dustFrac=dustFrac)

    plt.figure(1)
    plt.plot(stand_val['x'], stand_val['p'], linewidth=1.5, color="C0", linestyle='-', label="standard")
    plt.plot(frac_val['x'], frac_val['p'], linewidth=1.5, color="C0", linestyle='--',
             label="dust fraction: {}".format(dustFrac))
    plt.ylabel('pressure')
    plt.xlabel('x')
    plt.legend()
    plt.show()

    plt.figure(2)
    plt.plot(stand_val['x'], stand_val['rho'], linewidth=1.5, color="C1", linestyle='-', label="standard")
    plt.plot(frac_val['x'], frac_val['rho'], linewidth=1.5, color="C1", linestyle='--',
             label="dust fraction: {}".format(dustFrac))
    plt.ylabel('density')
    plt.xlabel('x')
    plt.legend()
    plt.show()

    plt.figure(3)
    plt.plot(stand_val['x'], stand_val['u'], linewidth=1.5, color="C2", linestyle='-', label="standard")
    plt.plot(frac_val['x'], frac_val['u'], linewidth=1.5, color="C2", linestyle='--',
             label="dust fraction: {}".format(dustFrac))
    plt.ylabel('velocity')
    plt.legend()
    plt.xlabel('x')
    plt.show()


if __name__ == '__main__':
    standard_sod_shock_tube()
    # combine_standard_dust_fraction_shock_tube()
